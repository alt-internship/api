from flask import Flask, redirect


app = Flask(__name__)

@app.route('/')
def api():
    return redirect('https://api.<project_name>.dandi-poc-1.poc-dev.com/ui')


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8888, debug=True)